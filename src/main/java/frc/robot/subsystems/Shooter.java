
package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj2.command.Subsystem;
import frc.robot.Robot;
import frc.robot.commands.shooter.StopShooting;
import frc.robot.sensors.Lidar;

public class Shooter implements Subsystem {
   
    WPI_TalonSRX shootingMotorOne = new WPI_TalonSRX(RobotMap.SHOOTING_MOTOR_ONE);
    WPI_TalonSRX shootingMotorTwo = new WPI_TalonSRX(RobotMap.SHOOTING_MOTOR_TWO);
    WPI_TalonSRX turretMotor = new WPI_TalonSRX(RobotMap.TURRET_MOTOR);
    WPI_TalonSRX loadingMotor = new WPI_TalonSRX(RobotMap.LOADING_MOTOR);
    //add limelight sensor 
    Lidar distanceSensor = new Lidar(RobotMap.LIDAR_PORT);
    DigitalInput ballLoaded = new DigitalInput(RobotMap.SHOOTER_LIMIT_SWITCH);

    SpeedControllerGroup shootingMotors = new SpeedControllerGroup(shootingMotorOne,shootingMotorTwo);

    public Shooter(){
        Robot.scheduler.registerSubsystem(this);
        Robot.scheduler.setDefaultCommand(this,new StopShooting(this));
        shootingMotorOne.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Relative);
    }
    
    public void setShootingSpeed(double speed){
        shootingMotors.set(speed);
    }

    public void loadNextBall(){

    }

    public boolean hasBall(){
        return ballLoaded.get();
    }

    private double getRampSpeed(){
        //turn distance from lidar into ramp speed for wheels on a -1 to 1 range
        return .5;
    }

    /*
    *@return The wheel "speed" in rpm
    */
    public double getWheelSpeed(){
        return shootingMotorOne.getSelectedSensorVelocity()*600;
    }

    public void setRampSpeed(){
        // implement the PID calls to attempt to reach the ramp speed given by the lidar
    }
    

}