package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj2.command.Subsystem;

public class Collector implements Subsystem {

    WPI_TalonSRX lowerBelt = new WPI_TalonSRX(RobotMap.LOWER_BELT);
    WPI_TalonSRX upperSmallerBelt = new WPI_TalonSRX(RobotMap.UPPER_SMALLER_BELT);
    WPI_TalonSRX upperLargerBelt = new WPI_TalonSRX(RobotMap.UPPER_LARGER_BELT);
    
    WPI_TalonSRX frontCollector = new WPI_TalonSRX(RobotMap.FRONT_COLLECTOR);
    WPI_TalonSRX backCollector = new WPI_TalonSRX(RobotMap.BACK_COLLECTOR);

    DoubleSolenoid frontCollectorGate = new DoubleSolenoid(RobotMap.FRONT_COLLECTOR_GATE);
    DoubleSolenoid backCollectorGate = new DoubleSolenoid(RobotMap.BACK_COLLECTOR_GATE);


    public void loadBall(){
        
    }

}