/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.ctre.phoenix.sensors.PigeonIMU;

import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.Talon;
import edu.wpi.first.wpilibj.VictorSP;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.Subsystem;
import frc.robot.Robot;
import frc.robot.RobotMap;
import frc.robot.commands.drivetrain.Drive;

/**
 * Add your docs here.
 */
public class Drivetrain implements Subsystem {
    
    PigeonIMU pigeon = new PigeonIMU(RobotMap.PIGEON_PORT);
    WPI_TalonSRX leftTopMotor = new WPI_TalonSRX(RobotMap.LEFT_TOP_MOTOR_PORT);
    WPI_TalonSRX leftMidMotor = new WPI_TalonSRX(RobotMap.LEFT_MID_MOTOR_PORT);
    WPI_TalonSRX leftBottomMotor = new WPI_TalonSRX(RobotMap.LEFT_BOTTOM_MOTOR_PORT);
    WPI_TalonSRX rightTopMotor = new WPI_TalonSRX(RobotMap.RIGHT_TOP_MOTOR_PORT);
    WPI_TalonSRX rightMidMotor = new WPI_TalonSRX(RobotMap.RIGHT_MID_MOTOR_PORT);
    WPI_TalonSRX rightBottomMotor = new WPI_TalonSRX(RobotMap.RIGHT_BOTTOM_MOTOR_PORT);
    private double metersPerTick=0.0001;
    private double x=0; //in meters
    private double y=0; //in meters
    //Test talon for the falcon500 motor
    WPI_TalonFX test = new WPI_TalonFX(0);
   
    SpeedControllerGroup leftMotors = new SpeedControllerGroup(leftTopMotor, leftMidMotor, leftBottomMotor);
    SpeedControllerGroup rightMotors = new SpeedControllerGroup(rightTopMotor,rightMidMotor, rightBottomMotor);
    DifferentialDrive drive = new DifferentialDrive(leftMotors, rightMotors);

    public Drivetrain(){
        Robot.scheduler.registerSubsystem(this);
        Robot.scheduler.setDefaultCommand(this, new Drive(this));
        leftMotors.setInverted(true);
    }

    public void periodic(){
        SmartDashboard.putNumber("encoder test",test.getSelectedSensorPosition());
       // test.configSelectedFeedbackSensor(FeedbackDevice.QuadEncoder); try using this line if encoder gets no feedback
       //otherwise try this one 
       //test.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MAGENCODER_ABSOLUTE);
        SmartDashboard.putNumber("control get", Robot.oi.getLeftJoystickY());
    }

    public void drive(double y, double z) {
        SmartDashboard.putNumber("talon 2 get", leftMotors.get());
        drive.arcadeDrive(y,z);
     }

    //updates X and Y position on the field when using TalonSRX's as the drive motor controllers
    public void updatePosition(){
        double averageTicks=(leftTopMotor.getSelectedSensorVelocity()+leftMidMotor.getSelectedSensorVelocity()+leftBottomMotor.getSelectedSensorVelocity()+
        rightTopMotor.getSelectedSensorVelocity()+rightMidMotor.getSelectedSensorVelocity()+rightBottomMotor.getSelectedSensorVelocity())/6;
        double distanceMoved=averageTicks*metersPerTick;
        double xHeading = getXHeading();
        x+= Math.cos(xHeading)*distanceMoved;
        y+= Math.sin(xHeading)*distanceMoved;
    }

    //returns x heading
    public double getXHeading(){
        double[] xyz = new double[3];
        pigeon.getRawGyro(xyz);
        return xyz[0];
    }

}
