package frc.robot.commands.collector;

import java.util.HashSet;
import java.util.Set;

import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.Subsystem;
import frc.robot.Robot;


public class LoadChamber implements Command {
    Set<Subsystem> requirements;
    
    public LoadChamber(){
        requirements = new HashSet<>();
        requirements.add(Robot.collector);
        requirements.add(Robot.shooter);
    }

    public Set<Subsystem> getRequirements(){
        return requirements;
    }

    public boolean hasRequirement(Subsystem s){
        return requirements.contains(s);
    }

    //Keep attempting to load the ball
    public void execute(){
        Robot.collector.loadBall();
    }
    
    //While the shooter does not have a ball in the chamber
    public boolean isFinished(){
        return Robot.shooter.hasBall();
    }

}