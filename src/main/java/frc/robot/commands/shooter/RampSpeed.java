package frc.robot.commands.shooter;

import java.util.HashSet;
import java.util.Set;

import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.Subsystem;

public class RampSpeed implements Command {
    Set<Subsystem> requirements;
    
    public RampSpeed(Subsystem ... s){
        requirements = new HashSet<>();
        for(Subsystem sub: s){
            requirements.add(sub);
        }
    }

    public Set<Subsystem> getRequirements(){
        return requirements;
    }
    
    public boolean hasRequirement(Subsystem s){
        return requirements.contains(s);
    }
    
    public void init(){
        new LoadBall().schedule();
        
    }

    public boolean isFinished(){
        return false;
    }

}

}