package frc.robot.commands.shooter;

import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.CommandGroupBase;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.commands.collector.LoadChamber;

public class ShootAndReload extends SequentialCommandGroup {
    public ShootAndReload(){
        addCommands(
            new ShootOne(),
            new LoadChamber()
        );
    }
}