
package frc.robot.sensors;

import edu.wpi.first.wpilibj.SerialPort;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class Lidar extends SerialPort implements Runnable {

    private static int dataBit = 8;
    private static int stopBit = 1;
    private byte[] packet = new byte[9];

    public Lidar() {
        super(115200, Port.kMXP, dataBit, Parity.kNone, StopBits.kOne);
        //Baud Rate, Port #, Data Bit, Parity, Stop Bit
        setReadBufferSize(9);
    }

    private short combine(byte one, byte two) {
        return (short) ((one & 0xFF) | (two << 8));
    }

    @Override
    public void run() {
        while(!Thread.interrupted()){
            packet = read(9);
                for (int x = 0; x < packet.length - 1; x++) {
                    if (packet[x] == 0x59 && packet[x + 1] == 0x59) {
                        if (x < 5) {
                            SmartDashboard.putBoolean("pased check", true);
                            SmartDashboard.putNumber("Lidar distance", combine(packet[x + 2], packet[x + 3]));
                            SmartDashboard.putString("Lidar one", Integer.toHexString(packet[x+2]));
                            SmartDashboard.putString("Lidar two", Integer.toHexString(packet[x+3]));

                        } else
                            SmartDashboard.putBoolean("pased check", false);
                    }
                }
        }
    }
}
