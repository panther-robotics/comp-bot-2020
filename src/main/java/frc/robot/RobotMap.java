/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

/**
 * Add your docs here.
 */
public class RobotMap {

    public static final int XBOX_PORT = 3;
    
    public static final int LEFT_TOP_MOTOR_PORT = 2;
    public static final int LEFT_MID_MOTOR_PORT = 3;
    public static final int LEFT_BOTTOM_MOTOR_PORT = 4;
    public static final int RIGHT_TOP_MOTOR_PORT = 5;
    public static final int RIGHT_MID_MOTOR_PORT = 6;
    public static final int RIGHT_BOTTOM_MOTOR_PORT = 7;
}
