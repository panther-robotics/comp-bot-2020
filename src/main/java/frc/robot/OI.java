package frc.robot;

import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj2.command.button.Button;
import frc.robot.commands.shooter.LoadBall;
import frc.robot.commands.shooter.Shoot;
import frc.robot.commands.shooter.ShootOne;

public class OI {

    XboxController xbox = new XboxController(RobotMap.XBOX_PORT);
    Button example = new Button(xbox,RobotMap.BUTTON_PORT);
    private static final double DEADBAND = 0.2;
    
    public double getLeftJoystickY() {
        double LeftY = xbox.getY(Hand.kLeft);
        //changes
        return applyDeadband(LeftY);
    }


    public double getLeftJoystickX() {
        double LeftX = xbox.getX(Hand.kLeft);
        //changes
        return applyDeadband(LeftX);
    }
    

    private double applyDeadband(double original) {
        if(Math.abs(original)<DEADBAND) {
            return 0;
        
        } else {
            return original;
        }
    }

    public OI(){
        example.whenPressed(new LoadBall().andThen(new ShootOne()));
    }
}


